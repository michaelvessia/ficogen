const questions = [
  { 

    question: "How many credit cards do you have?",
  
    answers: [
      "I have never had a credit card",
      "1",
      "2-4",
      "5 or more"
    ]
  
  }, 
    {
  
      question: "How long ago did you get your first loan? (i.e., auto loan, mortgage, student loan, etc.)",
      answers: [
        "I have never had a loan",
        "less than 6 months ago",
        "between 6 months and 2 years ago",
        "2 to 5 years ago",
        "5 to 10 years ago",
        "10 to 15 years ago",
        "15 to 20 years ago",
        "more than 20 years ago"
      ]
  
    }, 
    { 
  
      question: "How many loans or credit cards have you applied for in the last year?",
      answers: [
        "0 ",
        "1",
        "2",
        "3 to 5",
        "6 or more"
      ]
  
    }, 
    { 
  
      question: "How recently have you opened a new loan or credit card?",
      answers: [
        "less than 3 months ago",
        "between 3 and 6 months ago",
        "more than 6 months ago"
      ]
  
    }, 
    { 
  
      question: "How many of your loans and/or credit cards currently have a balance?",
      answers: [
        "0 to 4",
        "5 to 6",
        "7 to 8",
        "9 or more"
      ]
  
    }, 
    { 
  
      question: "Besides any mortgage loans, what are your total balances on all other loans and credit cards combined?",
      answers: [
        "I have only mortgage loan(s)",
        "Less than $500",
        "$500 to $999",
        "$1,000 to $4,999",
        "$5,000 to $9,999",
        "$10,000 to $19,999",
        "$20,000+",
      ]
  
    }, 
    { 
  
      question: "When did you last miss a loan or credit card payment?",
      answers: [
        "I have never missed a payment",
        "in the past 3 months",
        "3 to 6 months ago",
        "6 months to one year ago",
        "one to two years ago",
        "two to three years ago",
        "three to four years ago",
        "more than four years ago"
      ]	
    }, 
    { 
  
      question: "How many of your loans and/or credit cards are currently past due?",
      answers: [
  
        "0",
        "1",
        "2 or more"
      ]
  
    }, 
    { 
  
      question: "What percent of your total credit card limits do your credit card balances represent?",
      answers: [
        "I have never had a credit card",
        "0 to 9%",
        "10 to 19%",
        "20 to 29%",
        "30 to 39%",
        "40 to 49%",
        "50 to 59%",
        "60 to 69%",
        "70 to 79%",
        "80 to 89%",
        "90 to 99%",
        "100% or higher"
      ]
  
    }, 
    { 
  
      question: "In the last 10 years, have you ever experienced bankruptcy, repossession or an account in collections?",
      answers: [
        "Yes",
        "No"
      ]
    }
  
]
  

const getRandomAnswer = (questionLength) => {
  return Math.floor((Math.random() * questionLength));
}

//const randomAnswers = questionLengths.map(getRandomAnswer);

for(let i = 1; i < 30; i++) {

  console.log(`Student Profile #${i}`);

questions.forEach((question, j) => {
  console.log(`#${j+1} ${question.question}`);

  console.log(`- ${question.answers[getRandomAnswer(question.answers.length)]}`);
    console.log("");
})

console.log(" ------- ");


  
}
